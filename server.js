const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
const cors = require('cors')

const users = require('./routes/api/users')
const dishes = require('./routes/api/dishes')
const customers = require('./routes/api/customers')
const orders = require('./routes/api/orders')
const payments = require('./routes/api/payments')
const products = require('./routes/api/products')
const recipes = require('./routes/api/recipes')
const categories = require('./routes/api/categories')

const app = express()

// CORS middleware
// app.use(cors(corsOptions))
app.use(cors())
// public folder
app.use(express.static('public'))
// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// DB config
const db = require('./config/keys').mongoURI

// // Connect to MongoDB
// mongoose
//   .connect(
//     db
//     // { useNewUrlParser: true }
//   )
//   .then(() => console.log('MongoDB Connected'))
//   .catch(err => console.log(err))

mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: false })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => console.log(err))

// Passport middleware
app.use(passport.initialize())

// Passport config
require('./config/passport')(passport)

// Use Routes
app.use('/api/users', users)
app.use('/api/dishes', dishes)
app.use('/api/customers', customers)
app.use('/api/orders', orders)
app.use('/api/payments', payments)
app.use('/api/products', products)
app.use('/api/recipes', recipes)
app.use('/api/categories', categories)
app.use('/api/menus', require('./routes/api/menus'))

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server running on port ${port}`))
