const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load Models
const Recipe = require('../../models/Recipe');

// Load Validations
const validateRecipeInput = require('../../validation/recipe');

// @route   GET api/recipes/test
// @desc    Test recipes route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'recipes works' }));

// @route   POST api/recipes/all
// @desc    Get all recipe
// @access  Private
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Recipe.find()
      .sort({ title: 1 })
      .then(recipes => res.json(recipes))
      .catch(err => res.status(404).json({ norecipes: 'Нет рецептов', err }));
  }
);

// @route   POST api/recipes
// @desc    Create recipe
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    console.log(req.body);
    const { errors, isValid } = validateRecipeInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newRecipe = new Recipe({
      title: req.body.title,
      category: req.body.category,
      cost: req.body.cost,
      value: req.body.value,
      comment: req.body.comment,
      content: req.body.content,
      updated: Date.now(),
      author: req.user.id
    });

    // console.log('recipe', newRecipe);
    // res.json({ ok: 'ok' });
    newRecipe.save().then(item => res.json(item));
  }
);

// @route   POST api/products/:id
// @desc    Get product by id
// @access  Public
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    console.log('recipe')
    Recipe.findById(req.params.id)
      .then(item => res.json(item))
      .catch(err =>
        res.status(404).json({ noproduct: 'Нет такого рецепта', err })
      );
  }
);

// @route   PUT api/products
// @desc    Update product by id
// @access  Private
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProductInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const productID = req.body._id;
    if (productID) {
      Product.findById(productID)
        .then(product => {
          if (product) {
            const productFields = {
              title: req.body.title,
              category: req.body.category,
              isComplex: req.body.isComplex,
              value: req.body.value,
              amount: req.body.amount,
              price: req.body.price,
              waste: req.body.waste,
              comment: req.body.comment,
              content: req.body.content
            };
            Product.findByIdAndUpdate(
              productID,
              { $set: productFields },
              { new: true }
            )
              .then(product => {
                console.log(product);
                res.json(product);
              })
              .catch(err =>
                res.status(401).json({ errupdate: 'product not found' })
              );
          }
        })
        .catch(err => res.status(401).json({ errupdate: 'product not found' }));
    }
  }
);

// @route   DELETE api/recipes/:id
// @desc    Delete recipe by id
// @access  Privite
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete dish' });
    }

    Recipe.findById(req.params.id)
      .then(item => {
        // Delete
        item.remove().then(item => res.json(item));
      })
      .catch(err => res.status(404).json({ norecipe: 'No recipe found' }));
  }
);

module.exports = router;
