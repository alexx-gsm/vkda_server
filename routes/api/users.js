const express = require('express')
const router = express.Router()
const gravatar = require('gravatar')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
const passport = require('passport')

// Load Input Validation
const validateRegisterInput = require('../../validation/register')
const validateLoginInput = require('../../validation/login')
const validateUserInput = require('../../validation/user')

// Load User Model
const User = require('../../models/User')

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body)

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors)
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = 'Email already exists'

      return res.status(400).json(errors)
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm', // Default
      })

      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        phone: req.body.phone,
        role: 'user',
        isMember: req.body.isMember,
        mentorId: req.body.mentorId,
        avatar,
      })

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err
          newUser.password = hash
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => console.log(err))
        })
      })
    }
  })
})

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body)

  console.log('login')

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors)
  }

  const email = req.body.email
  const password = req.body.password

  // Find User by email
  User.findOne({ email }).then((user) => {
    // Check fo User
    if (!user) {
      errors.email = 'User not found'
      return res.status(404).json(errors)
    }

    // Check password
    bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        // User matched

        const payload = {
          // create JWT payload
          id: user.id,
          name: user.name,
          avatar: user.avatar,
          role: user.role,
          email: user.email,
          phone: user.phone,
        }

        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600000 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token,
            })
          }
        ) // 1 hour
      } else {
        errors.password = 'Password incorrect'
        return res.status(400).json(errors)
      }
    })
  })
})

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json(req.user)
  }
)

/**
 *! @route   POST api/users
 ** @desc    Save/Update user (from admin)
 ** @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateUserInput(req.body)
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body
    const itemFields = {
      name: req.body.name,
      company: req.body.company,
      phone: req.body.phone,
      address: req.body.address,
      map: req.body.map,
      comment: req.body.comment,
      role: 'user',
    }

    try {
      if (_id && req.user.role === 'admin') {
        res.json(
          await User.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else if (req.user.role === 'admin') {
        const newItem = new User(itemFields)
        res.json(await newItem.save())
      } else {
        res.status(403).json('ops')
      }
    } catch (error) {
      console.log('error', error)
      res.json(error)
    }
  }
)

/**
 *! @route   POST api/users/all
 ** @desc    Get all users
 ** @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role === 'admin') {
        res.json(await User.find({ isDeleted: false }).sort({ name: 1 }))
      } else if (req.user.role === 'user') {
        res.json(await User.findOne({ id: req.user.id }))
      }
    } catch (error) {
      console.log('error', error)
      res.json(error)
    }
  }
)

/**
 *! @route   POST api/customer/:id
 ** @desc    Get user by id
 ** @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params
    try {
      if (_id && req.user.role === 'admin') {
        res.json(await User.findOne({ _id }))
      } else if (req.user.role === 'user') {
        res.json(await User.findOne({ id: req.user.id }))
      }
    } catch (error) {
      console.log('error', error)
      res.status(404).json(error)
    }
  }
)

/**
 *! @route   DELETE api/customer/:id
 ** @desc    Delete user (mark user)
 ** @access  Private
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const { _id } = req.params
      if (_id && req.user.role === 'admin') {
        await User.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
        res.json('ok')
      } else {
        res.status(404).json('ops')
      }
    } catch (error) {
      console.log('error', error)
      res.status(404).json(error)
    }
  }
)

module.exports = router
