const express = require('express')
const router = express.Router()
const passport = require('passport')

// Load Models
const Category = require('../../models/Category')

// Load Validations
const validateCategoryInput = require('../../validation/category')

// @route   GET api/categories/test
// @desc    Test category route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'category Works' }))

/**
 * ! @route   POST api/categories
 * ? @desc    Create category
 * * @access  Private
 */
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    console.log('---')
    const { errors, isValid } = validateCategoryInput(req.body)
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    try {
      const itemFields = {
        title: req.body.title,
        sorting: req.body.sorting,
        isRoot: req.body.isRoot,
        rootId: req.body.rootId,
      }

      const { _id } = req.body

      if (_id) {
        res.json(
          await Category.findOneAndUpdate(
            { _id },
            { $set: itemFields },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Category(itemFields)
        res.json(await newItem.save())
      }
    } catch (error) {
      console.log('error', error)
      res.status(400).json(error)
    }
  }
)

// @route   PUT api/categories
// @desc    Update category by id
// @access  Private
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateCategoryInput(req.body)
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const id = req.body._id
    if (id) {
      Category.findById(id)
        .then(item => {
          if (item) {
            const fields = {
              title: req.body.title,
              isRoot: req.body.isRoot,
              rootId: req.body.rootId,
              sorting: req.body.sorting,
            }
            Category.findByIdAndUpdate(id, { $set: fields }, { new: true })
              .then(item => {
                res.json(item)
              })
              .catch(err =>
                res.status(401).json({ errupdate: 'category not found', err })
              )
          }
        })
        .catch(err =>
          res.status(401).json({ errupdate: 'category not found', err })
        )
    }
  }
)

// @route   POST api/categories/all
// @desc    Get all categories
// @access  Private
router.post(
  '/all',
  // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Category.find()
      .sort({ sorting: 1 })
      .then(items => res.json(items))
      .catch(err =>
        res.status(404).json({ nocategories: 'Нет категорий yet', err })
      )
  }
)

// @route   POST api/categories/:id
// @desc    Get category by id
// @access  Private
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Category.findById(req.params.id)
      .then(item => res.json(item))
      .catch(err =>
        res.status(404).json({ nocategory: 'Нет такой категории', err })
      )
  }
)

module.exports = router
