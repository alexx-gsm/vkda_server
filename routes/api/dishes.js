const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const passport = require('passport')

const multer = require('multer')
const uploadDest = multer({ dest: 'uploads/' })
const fs = require('fs')

// Load Models
const Dish = require('../../models/Dish')

// Load Validations
const validateDishInput = require('../../validation/dish')

/**
 * ! @route   POST api/dishes
 * ? @desc    Save dish
 * * @access  Private
 */
router.post(
  '/',
  [
    passport.authenticate('jwt', { session: false }),
    uploadDest.single('upload')
  ],
  (req, res) => {
    const { errors, isValid } = validateDishInput(req.body)

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { upload } = req.body
    let { image } = req.body

    if (upload) {
      const base64image = upload[0].replace(/^data:image\/jpeg;base64,/, '')
      image = `/images/dish-${Date.now()}.jpeg`

      fs.writeFile(`public${image}`, base64image, 'base64', function(err) {
        console.log(err)
      })
    }

    const dishId = req.body._id || null

    const dishFields = {
      image,
      title: req.body.title,
      categoryId: req.body.categoryId,
      price: req.body.price,
      price2: req.body.price2,
      weight: req.body.weight,
      weight2: req.body.weight2,
      recipe: req.body.recipe,
      comment: req.body.comment
    }

    if (dishId) {
      Dish.findById(dishId).then(dish => {
        if (dish) {
          Dish.findByIdAndUpdate(dishId, { $set: dishFields }, { new: true })
            .then(dish => res.json(dish))
            .catch(err =>
              res.status(401).json({ nodish: 'Нет такого dish', err: err })
            )
        }
      })
    } else {
      const newDish = new Dish(dishFields)
      newDish.save().then(dish => res.json(dish))
    }
  }
)

/**
 * ! @route   POST api/dishes
 * * @desc    Get all dishes
 * ? @access  Private
 */
router.post(
  '/all',
  // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Dish.find()
      .sort({ title: 1 })
      .then(items => res.json(items))
      .catch(err => res.status(404).json({ nodishes: 'Нету блюд' }))
  }
)

// @route   GET api/dishes/:id
// @desc    Get dish by id
// @access  Public
router.post('/:id', (req, res) => {
  Dish.findById(req.params.id)
    .then(item => res.json(item))
    .catch(err => res.status(404).json({ nodish: 'Нет такого блюда' }))
})

/**
 *! @route   DELETE api/dishes/:id
 ** @desc    Delete dish by id
 *? @access  Privite
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete dish' })
    }

    const { _id } = req.params

    try {
      if (_id) {
        await Dish.deleteOne({ _id })
        res.json({})
      }
    } catch (error) {
      res.status(403).json(error)
    }
  }
)

module.exports = router
