const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load Models
const Product = require('../../models/Product');

// Load Validations
const validateProductInput = require('../../validation/product');

// @route   GET api/payments/test
// @desc    Test payments route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Products Works' }));

// @route   POST api/products/all
// @desc    Get all product
// @access  Private
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Product.find()
      .sort({ title: 1 })
      .then(products => res.json(products))
      .catch(err => res.status(404).json({ noproducts: 'Нет продуктов', err }));
  }
);

// @route   POST api/products
// @desc    Create product
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    console.log(req.body);
    const { errors, isValid } = validateProductInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newProduct = new Product({
      title: req.body.title,
      category: req.body.category,
      isComplex: req.body.isComplex,
      value: req.body.value,
      amount: req.body.amount,
      price: req.body.price,
      waste: req.body.waste,
      comment: req.body.comment,
      content: req.body.content
    });

    newProduct.save().then(item => res.json(item));
  }
);

// @route   POST api/products/:id
// @desc    Get product by id
// @access  Private
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Product.findById(req.params.id)
      .then(item => res.json(item))
      .catch(err =>
        res.status(404).json({ noproduct: 'Нет такого продукта', err })
      );
  }
);

// @route   PUT api/products
// @desc    Update product by id
// @access  Private
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProductInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const productID = req.body._id;
    if (productID) {
      Product.findById(productID)
        .then(product => {
          if (product) {
            const productFields = {
              title: req.body.title,
              category: req.body.category,
              isComplex: req.body.isComplex,
              value: req.body.value,
              amount: req.body.amount,
              price: req.body.price,
              waste: req.body.waste,
              comment: req.body.comment,
              content: req.body.content
            };
            Product.findByIdAndUpdate(
              productID,
              { $set: productFields },
              { new: true }
            )
              .then(product => {
                console.log(product);
                res.json(product);
              })
              .catch(err =>
                res.status(401).json({ errupdate: 'product not found' })
              );
          }
        })
        .catch(err => res.status(401).json({ errupdate: 'product not found' }));
    }
  }
);

// @route   DELETE api/products/:id
// @desc    Delete product by id
// @access  Privite
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete dish' });
    }

    Product.findById(req.params.id)
      .then(item => {
        // Delete
        item.remove().then(item => res.json(item));
      })
      .catch(err => res.status(404).json({ nodish: 'No dish found' }));
  }
);

module.exports = router;
