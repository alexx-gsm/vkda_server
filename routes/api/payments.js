const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const passport = require('passport')
const moment = require('moment')

// Load Models
const Payment = require('../../models/Payment')

// Load Validations
const validatePaymentInput = require('../../validation/payment')

// @route   POST api/payments
// @desc    Create payment
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePaymentInput(req.body)
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const newPayment = new Payment({
      date: req.body.date,
      week: req.body.week,
      title: req.body.title,
      variant: req.body.variant,
      type: req.body.type,
      isIncome: req.body.isIncome,
      cash: req.body.cash,
      card: req.body.card,
      comment: req.body.comment,
      user: req.user.id,
      isTransfer: req.body.isTransfer,
      from: req.body.from,
      to: req.body.to,
      isFromCard: req.body.isFromCard,
      isToCard: req.body.isToCard,
      transferAmount: req.body.transferAmount,
    })

    newPayment.save().then((item) => res.json(item))
  }
)

// @route   POST api/payments/all
// @desc    Get all payments
// @access  Private
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Payment.find()
      .sort({ date: -1 })
      .then((payments) => res.json(payments))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/week/:num
// @desc    Get all payments in $num week
// @access  Private
router.post(
  '/week/:num',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Payment.find({ week: req.params.num })
      .sort({ date: -1 })
      .then((payments) => res.json(payments))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/day
// @desc    Get all payments in day
// @access  Private
router.post(
  '/day',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const day = req.body.day
    const selectedDay = moment(day)
      .startOf('day')
      .toDate()

    const selectedNextDay = moment(day)
      .startOf('day')
      .add(1, 'd')
      .toDate()
    // res.json({ ok: 'ok' });
    Payment.find({
      date: {
        $gte: selectedDay,
        $lt: selectedNextDay,
      },
    })
      .sort({ date: -1 })
      .then((payments) => res.json(payments))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/year
// @desc    Get all payments in year
// @access  Private
router.post(
  '/year',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const year = req.body.year

    Payment.find({
      date: {
        $gte: moment([year]),
        $lt: moment([year]).endOf('year'),
      },
    })
      .sort({ date: -1 })
      .then((payments) => res.json(payments))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/month
// @desc    Get all payments in month
// @access  Private
router.post(
  '/month',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const year = req.body.year
    const month = req.body.month

    console.log('year', year)
    console.log('month', month)

    Payment.find({
      date: {
        $gte: moment([year, month]),
        $lt: moment([year, month]).endOf('month'),
      },
    })
      .sort({ date: -1 })
      .then((payments) => res.json(payments))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/count
// @desc    Get payment count in the same day
// @access  Private
router.post(
  '/count',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const day = req.body.day
    const selectedDay = moment(day)
      .startOf('day')
      .toDate()

    const selectedNextDay = moment(day)
      .startOf('day')
      .add(1, 'd')
      .toDate()

    Payment.find({
      date: {
        $gte: selectedDay,
        $lt: selectedNextDay,
      },
    })
      .then((count) => res.json(count.length))
      .catch((err) => res.status(404).json({ nopayments: 'Нет движухи', err }))
  }
)

// @route   POST api/payments/:id
// @desc    Get payment by id
// @access  Public
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Payment.findById(req.params.id)
      .then((item) => res.json(item))
      .catch((err) => res.status(404).json({ nopayment: 'Нет такого чека' }))
  }
)

// @route   PUT api/payments
// @desc    Update payment by id
// @access  Private
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePaymentInput(req.body)
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors)
    }

    const paymentID = req.body._id
    if (paymentID) {
      Payment.findById(paymentID)
        .then((payment) => {
          if (payment) {
            const paymentFields = {
              date: req.body.date,
              week: req.body.week,
              title: req.body.title,
              variant: req.body.variant,
              type: req.body.type,
              isIncome: req.body.isIncome,
              cash: req.body.cash,
              card: req.body.card,
              comment: req.body.comment,
              user: req.user.id,
              isTransfer: req.body.isTransfer,
              from: req.body.from,
              to: req.body.to,
              isFromCard: req.body.isFromCard,
              isToCard: req.body.isToCard,
              transferAmount: req.body.transferAmount,
            }
            Payment.findByIdAndUpdate(
              paymentID,
              { $set: paymentFields },
              { new: true }
            )
              .then((payment) => {
                res.json(payment)
              })
              .catch((err) =>
                res.status(401).json({ errupdate: 'payment not found' })
              )
          }
        })
        .catch((err) =>
          res.status(401).json({ errupdate: 'payment not found' })
        )
    }
  }
)

// @route   DELETE api/payments/:id
// @desc    Delete payment by id
// @access  Privite
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // if (req.user.role !== 'admin') {
    //   return res.status(401).json({ noadmin: 'only admin can delete dish' })
    // }

    Payment.findById(req.params.id)
      .then((item) => {
        // Delete
        item.remove().then((item) => res.json(item))
      })
      .catch((err) => res.status(404).json({ nodish: 'No dish found' }))
  }
)

module.exports = router
