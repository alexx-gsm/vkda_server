const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create UserSchema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    // required: true,
  },
  phone: {
    type: String,
    // required: true,
  },
  password: {
    type: String,
    // required: true,
  },
  avatar: {
    type: String,
    default: '',
  },
  role: {
    type: String,
    required: true,
  },
  isMember: {
    type: Boolean,
    default: false,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  mentorId: {
    type: String,
    default: '',
  },
  comment: {
    type: String,
    default: '',
  },
  date: {
    type: Date,
    default: Date.now,
  },
})

module.exports = User = mongoose.model('user', UserSchema)
