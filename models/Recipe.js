const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create ProductSchema
const RecipeSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  cost: {
    type: Number,
    default: 0
  },
  comment: {
    type: String
  },
  content: [
    {
      product_id: {
        type: Schema.Types.ObjectId,
        ref: 'product'
      },
      weight: {
        type: Number,
        default: 1
      },
      shrinkage: {
        type: Number,
        default: 0
      }
    }
  ],
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

module.exports = Recipe = mongoose.model('recipe', RecipeSchema);
