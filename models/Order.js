const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create OrderSchema
const OrderSchema = new Schema({
  deliveryDate: {
    type: String,
    required: true,
  },
  number: {
    type: String,
    required: true,
  },
  customer: {
    type: String,
    required: true,
  },
  dishes: [
    {
      dish: {
        type: Schema.Types.ObjectId,
        ref: 'dish'
      },
      amount: {
        type: Number,
        require: true
      },
      is_ready: {
        type: Boolean,
        default: false
      }
    }
  ],
  // name: {
  //   type: String,
  //   required: true,
  // },
  // phone: {
  //   type: String,
  //   required: true,
  // },
  // address: {
  //   type: String,
  //   required: true,
  // },
  // cart: {
  //   type: Object,
  //   required: true,
  // },
  total: {
    type: String,
  },
  courier: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  status: {
    type: String,
    default: 'new',
  },
  isPaid: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  comment: {
    type: String,
  },
})

module.exports = Order = mongoose.model('order', OrderSchema)
