const mongoose = require('mongoose')
const Scheme = mongoose.Schema

// Create Category Scheme
const CategoryScheme = new Scheme({
  title: {
    type: String,
    required: true,
  },
  isRoot: {
    type: Boolean,
    default: false,
  },
  rootId: {
    type: String,
  },
  sorting: {
    type: Number,
    default: 0,
  },
})

module.exports = Category = mongoose.model('category', CategoryScheme)
