const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MenuSchema = new Schema({
  date: {
    type: String,
    required: true,
  },
  week: {
    type: Number,
    require: true,
  },
  dishes: [
    {
      type: Schema.Types.ObjectId,
      ref: 'dishes',
    },
  ],
  link: {
    type: String,
    default: '',
  },
  comment: {
    type: String,
    default: '',
  },
})

module.exports = Menu = mongoose.model('menu', MenuSchema)
