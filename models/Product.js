const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create ProductSchema
const ProductSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  isComplex: {
    type: Boolean,
    default: false
  },
  value: {
    type: Number,
    default: 1
  },
  amount: {
    type: Number,
    default: 1
  },
  price: {
    type: Number,
    default: 0
  },
  waste: {
    type: Number,
    default: 0
  },
  comment: {
    type: String
  },
  content: [
    {
      product_id: {
        type: Schema.Types.ObjectId,
        ref: "product"
      },
      amount: {
        type: Number,
        default: 1
      }
    }
  ]
});

module.exports = Product = mongoose.model("product", ProductSchema);
