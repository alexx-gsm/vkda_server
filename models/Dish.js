const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create Schema
const DishSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  weight: {
    type: String
  },
  weight2: {
    type: String
  },
  price: {
    type: String,
    required: true
  },
  price2: {
    type: String,
    default: ''
  },
  categoryId: {
    type: String
    // required: true
  },
  comment: {
    type: String
  },
  recipe: {
    type: String
  },
  image: {
    type: String
  },
  images: [
    {
      type: String
    }
  ],
  date: {
    type: Date,
    default: Date.now
  }
  // components: [
  //   {
  //     component: {
  //       type: Schema.Types.ObjectId,
  //       ref: 'components'
  //     },
  //     amount: {
  //       type: String
  //     }
  //   }
  // ],
  // cost: {
  //   type: String
  // }
})

module.exports = Dish = mongoose.model('dish', DishSchema)
