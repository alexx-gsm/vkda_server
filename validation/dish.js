const Validator = require('validator')
const isEmpty = require('./is-empty')

module.exports = function validatePostInput(data) {
  let errors = {}

  data.title = !isEmpty(data.title) ? data.title : ''

  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название блюда от 3 до 50 символов'
  }

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // Category
  data.categoryId = !isEmpty(data.categoryId) ? data.categoryId : ''
  if (Validator.isEmpty(data.categoryId)) {
    errors.categoryId = 'Категория обязательна'
  }

  // price
  data.price = !isEmpty(data.price) ? data.price : ''
  if (Validator.isEmpty(data.price)) {
    errors.price = 'Цена обязательна'
  }

  // price 2
  data.price2 = !isEmpty(data.price2) ? data.price2 : data.price

  // Weight
  data.weight = !isEmpty(data.weight) ? data.weight : ''

  // Weight2
  data.weight2 = !isEmpty(data.weight2) ? data.weight2 : data.weight

  // Recipe
  data.recipe = !isEmpty(data.recipe) ? data.recipe : ''

  // Comment
  data.comment = !isEmpty(data.comment) ? data.comment : ''

  // TODO: all reqirued field validation

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
