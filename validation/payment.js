const Validator = require('validator')
const isEmpty = require('./is-empty')

module.exports = function validatePaymentInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  // if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
  //   errors.title = 'Название от 3 до 50 символов'
  // }
  // if (Validator.isEmpty(data.title)) {
  //   errors.title = 'Название продукта обязательно'
  // }

  // isTransfer
  data.isTransfer = !isEmpty(data.isTransfer) ? data.isTransfer : false
  // from
  data.from = !isEmpty(data.from) ? data.from : 'vkda'
  // to
  data.to = !isEmpty(data.to) ? data.to : 'colibri'
  // transferAmount
  data.transferAmount = !isEmpty(data.transferAmount) ? data.transferAmount : 0
  // isFromCard
  data.isFromCard = !isEmpty(data.isFromCard) ? data.isFromCard : true
  // isToCard
  data.isToCard = !isEmpty(data.isToCard) ? data.isToCard : false

  // variant
  data.variant = !isEmpty(data.variant) ? data.variant : 'colibri'
  // type
  data.type = !isEmpty(data.type) ? data.type : ''
  if (!data.isTransfer && Validator.isEmpty(data.type)) {
    errors.type = 'Тип документа обязателен'
  }

  // week
  data.week = !isEmpty(data.week) ? data.week.toString() : ''
  if (Validator.isEmpty(data.week)) {
    errors.week = 'where week number?'
  }

  // summury

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
