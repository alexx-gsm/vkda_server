const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateOrderInput(data) {
  let errors = {};

  // CUSTOMER
  data.customer = !isEmpty(data.customer) ? data.customer : '';
  if (Validator.isEmpty(data.customer)) {
    errors.customer = 'Необходимо указать клиента';
  }

  // STATUS
  data.status = !isEmpty(data.status) ? data.status : 'draft';

  // NUMBER
  data.number = !isEmpty(data.number) ? data.number : '';
  if (!Validator.isLength(data.number, { min: 3, max: 50 })) {
    errors.number = 'NUMBER WRONG !!!';
  }
  if (Validator.isEmpty(data.number)) {
    errors.number = 'NUMBER WRONG !!!';
  }

  // DISHES
  data.dishes = !isEmpty(data.dishes) ? data.dishes : [];
  if (data.dishes.length === 0) {
    errors.dishes = 'Укажите блюда для заказа';
  }

  // COMMENT
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
