const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRecipeInput(data) {
  let errors = {};

  // title
  data.title = !isEmpty(data.title) ? data.title : '';
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название от 3 до 50 символов';
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название продукта обязательно';
  }

  // category
  data.category = !isEmpty(data.category) ? data.category : '';
  if (Validator.isEmpty(data.category)) {
    errors.category = 'Категория продукта обязательна';
  }

  // cost
  data.cost = !isEmpty(data.cost) ? +data.cost : 0;
  if (data.cost <= 0) {
    errors.cost = 'Стоимость должна быть больше 0';
  }

  data.content = !isEmpty(data.content) ? data.content : [];
  if (isEmpty(data.content)) {
    errors.content = 'Укажите состав рецепта';
  } else {
    data.content.map((row, i) => {
      if (row._id === '0') {
        errors.content.product.push({
          i,
          error: 'не должно быть пустой строки'
        });
      }
    });
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
