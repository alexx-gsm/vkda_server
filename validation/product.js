const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProductInput(data) {
  let errors = {};

  // title
  data.title = !isEmpty(data.title) ? data.title : '';
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название от 3 до 50 символов';
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название продукта обязательно';
  }

  // category
  data.category = !isEmpty(data.category) ? data.category : '';
  if (Validator.isEmpty(data.category)) {
    errors.category = 'Категория продукта обязательна';
  }

  // price
  data.price = !isEmpty(data.price) ? +data.price : 0;
  if (data.price <= 0) {
    errors.price = 'Цена продукта должна быть больше 0';
  }

  // waste
  data.waste = !isEmpty(data.waste) ? +data.waste : 0;
  if (data.waste < 0 || data.waste > 50) {
    errors.waste = 'Отходы должны быть в пределах 0-50%';
  }

  // isComplex
  data.isComplex = !isEmpty(data.isComplex) ? data.isComplex : false;

  // value
  data.value = !isEmpty(data.value) ? +data.value : 0;
  if (!data.isComplex && data.value <= 0) {
    errors.value = 'Необходимо указать стоимость продукта';
  }

  // amount
  data.amount = !isEmpty(data.amount) ? +data.amount : 0;
  if (!data.isComplex && data.amount <= 0) {
    errors.amount = 'Необходимо указать кол-во продукта';
  }

  // content
  data.content = !isEmpty(data.content) ? data.content : [];
  if (data.isComplex && isEmpty(data.content)) {
    errors.content = 'Укажите содержимое составного продукта';
  }
  data.content.map((row, index) => {
    if (row._id === '0') {
      errors.content = 'Пустая строка!';
    }
  });

  console.log('--errors', errors);
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
