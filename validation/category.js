const Validator = require('validator')
const isEmpty = require('./is-empty')

module.exports = function validateCategoryInput(data) {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название от 3 до 50 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название категории обязательно'
  }

  // isRoot
  data.isRoot = !isEmpty(data.isRoot) ? data.isRoot : false

  // rootId
  data.rootId = data.isRoot ? '' : !isEmpty(data.rootId) ? data.rootId : -1
  if (data.rootId === -1) {
    errors.rootId = 'Укажите родительскую категорию'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
